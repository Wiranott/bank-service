package com.example.bankservice.service;

import com.example.bankservice.dto.AccountDTO;
import com.example.bankservice.dto.ExchangeDTO;
import com.example.bankservice.exception.AccountAlreadyExistsException;
import com.example.bankservice.exception.InsufficientFundsException;
import com.example.bankservice.exception.ResourceNotFoundException;
import com.example.bankservice.model.Account;
import com.example.bankservice.model.Enum.Currency;
import com.example.bankservice.repository.AccountRepository;
import com.example.bankservice.strategy.ExchangeStrategyFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
public class AccountService {

    private final AccountRepository accountRepository;
    private final ExchangeStrategyFactory exchangeStrategyFactory;
    private final ExchangeRateService exchangeRateService;

    public Account createAccount(AccountDTO accountRequest) {
        if (accountRepository.existsById(accountRequest.getPesel())) {
            throw new AccountAlreadyExistsException("Account with this PESEL already exists");
        }
        var account = Account.createNewAccount(accountRequest);

        return accountRepository.save(account);
    }

    public Account getAccount(String pesel) {
        return accountRepository.findById(pesel).orElseThrow(() -> new ResourceNotFoundException("Account not found"));
    }

    public void exchangeCurrency(String pesel, ExchangeDTO exchangeRequest) {
        var account = getAccount(pesel);
        var fromCurrency = Currency.valueOf(exchangeRequest.getFromCurrency());
        var toCurrency = Currency.valueOf(exchangeRequest.getToCurrency());
        var rate = exchangeRateService.getExchangeRate(exchangeRequest.getFromCurrency(), exchangeRequest.getToCurrency());

        checkSufficientFunds(account, fromCurrency, exchangeRequest.getAmount());

        var strategy = exchangeStrategyFactory.getStrategy(fromCurrency, toCurrency);
        if (strategy != null) {
            strategy.exchange(account, exchangeRequest.getAmount(), rate);
            accountRepository.save(account);
        } else {
            throw new IllegalArgumentException("Invalid currency exchange pair");
        }
    }

    private void checkSufficientFunds(Account account, Currency fromCurrency, BigDecimal amount) {
        if (fromCurrency == Currency.PLN) {
            if (account.getBalancePLN().compareTo(amount) < 0) {
                throw new InsufficientFundsException("Insufficient funds in PLN account");
            }
        } else if (fromCurrency == Currency.USD) {
            if (account.getBalanceUSD().compareTo(amount) < 0) {
                throw new InsufficientFundsException("Insufficient funds in USD account");
            }
        } else {
            throw new IllegalArgumentException("Unsupported currency: " + fromCurrency);
        }
    }
}