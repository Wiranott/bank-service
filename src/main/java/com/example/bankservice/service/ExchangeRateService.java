package com.example.bankservice.service;

import com.example.bankservice.dto.ExchangeRateResponse;
import com.example.bankservice.model.Enum.Currency;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
public class ExchangeRateService {

    private final WebClient webClient;

    public BigDecimal getExchangeRate(String fromCurrency, String toCurrency) {
        var url = "/api/exchangerates/rates/A/" + (fromCurrency.equals(Currency.PLN.name()) ? toCurrency : fromCurrency);
        var response = webClient.get()
                .uri(url)
                .retrieve()
                .bodyToMono(ExchangeRateResponse.class)
                .block();


        if (response != null && !response.getRates().isEmpty()) {
            return response.getRates().getFirst().getMid();
        } else {
            throw new RuntimeException("Failed to retrieve exchange rate");
        }
    }
}
