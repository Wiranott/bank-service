package com.example.bankservice.dto;

import com.example.bankservice.validation.Adult;
import jakarta.validation.constraints.*;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class AccountDTO {

    @NotBlank(message = "First name is required")
    @Size(max = 255, message = "First name cannot be longer than 255 characters")
    private String firstName;

    @NotBlank(message = "Last name is required")
    @Size(max = 255, message = "Last name cannot be longer than 255 characters")
    private String lastName;

    @NotBlank(message = "PESEL is required")
    @Pattern(regexp = "\\d{11}", message = "PESEL must be 11 digits")
    @Adult(message = "Person must be an adult")
    private String pesel;

    @NotNull(message = "Initial balance in PLN is required")
    @Digits(integer = 18, fraction = 4, message = "Initial balance in PLN must be a valid decimal number with up to 18 integer digits and 4 fraction digits")
    private BigDecimal initialBalancePLN;
}
