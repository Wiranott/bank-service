package com.example.bankservice.dto;

import com.example.bankservice.model.Enum.Currency;
import com.example.bankservice.validation.ValidEnum;
import jakarta.validation.constraints.Digits;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class ExchangeDTO {

    @NotNull(message = "Amount is required")
    @Digits(integer = 18, fraction = 4, message = "amount be a valid decimal number with up to 18 integer digits and 4 fraction digits")
    private BigDecimal amount;

    @NotBlank(message = "From currency is required")
    @ValidEnum(enumClass = Currency.class, message = "From currency must be one of PLN or USD")
    private String fromCurrency;

    @NotBlank(message = "To currency is required")
    @ValidEnum(enumClass = Currency.class, message = "To currency must be one of PLN or USD")
    private String toCurrency;
}