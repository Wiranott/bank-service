package com.example.bankservice.dto;

import lombok.Getter;

import java.math.BigDecimal;

@Getter
public class Rate {
    private BigDecimal mid;
}
