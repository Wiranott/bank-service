package com.example.bankservice.dto;

import lombok.Getter;

import java.util.List;

@Getter
public class ExchangeRateResponse {

    private List<Rate> rates;
}
