package com.example.bankservice.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.time.LocalDate;

public class AdultValidator implements ConstraintValidator<Adult, String> {

    @Override
    public void initialize(Adult constraintAnnotation) {
    }

    @Override
    public boolean isValid(String pesel, ConstraintValidatorContext context) {
        if (pesel == null || !pesel.matches("\\d{11}")) {
            return false;
        }

        try {
            var birthDate = extractBirthDate(pesel);
            return birthDate.plusYears(18).isBefore(LocalDate.now()) || birthDate.plusYears(18).isEqual(LocalDate.now());
        } catch (Exception e) {
            return false;
        }
    }

    private LocalDate extractBirthDate(String pesel) throws NumberFormatException {
        var year = Integer.parseInt(pesel.substring(0, 2));
        var month = Integer.parseInt(pesel.substring(2, 4));
        var day = Integer.parseInt(pesel.substring(4, 6));

        if (month > 20 && month < 33) {
            month -= 20;
            year += 2000;
        } else if (month > 40 && month < 53) {
            month -= 40;
            year += 2100;
        } else if (month > 60 && month < 73) {
            month -= 60;
            year += 2200;
        } else {
            year += 1900;
        }

        return LocalDate.of(year, month, day);
    }
}
