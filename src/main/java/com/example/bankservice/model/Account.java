package com.example.bankservice.model;

import com.example.bankservice.dto.AccountDTO;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Entity
@Getter
@Setter
public class Account {

    @Id
    private String pesel;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "balance_pln")
    private BigDecimal balancePLN;

    @Column(name = "balance_usd")
    private BigDecimal balanceUSD;

    public static Account createNewAccount(AccountDTO accountRequest) {
        Account account = new Account();
        account.setPesel(accountRequest.getPesel());
        account.setFirstName(accountRequest.getFirstName());
        account.setLastName(accountRequest.getLastName());
        account.setBalancePLN(accountRequest.getInitialBalancePLN());
        account.setBalanceUSD(BigDecimal.ZERO);
        return account;
    }

}
