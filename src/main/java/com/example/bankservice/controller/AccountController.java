package com.example.bankservice.controller;

import com.example.bankservice.dto.AccountDTO;
import com.example.bankservice.dto.ExchangeDTO;
import com.example.bankservice.model.Account;
import com.example.bankservice.service.AccountService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/accounts")
public class AccountController {

    private final AccountService accountService;

    @PostMapping
    public ResponseEntity<Account> createAccount(@Valid @RequestBody AccountDTO accountRequest) {
        Account account = accountService.createAccount(accountRequest);
        return new ResponseEntity<>(account, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<Account> getAccount(@RequestParam String pesel) {
        Account account = accountService.getAccount(pesel);
        return ResponseEntity.ok(account);
    }

    @PostMapping("/exchange")
    public ResponseEntity<Void> exchangeCurrency(@RequestParam String pesel, @Valid @RequestBody ExchangeDTO exchangeRequest) {
        accountService.exchangeCurrency(pesel, exchangeRequest);
        return ResponseEntity.ok().build();
    }

}