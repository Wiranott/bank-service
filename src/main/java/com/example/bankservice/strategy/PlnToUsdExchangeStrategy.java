package com.example.bankservice.strategy;

import com.example.bankservice.model.Account;

import java.math.BigDecimal;
import java.math.RoundingMode;


public class PlnToUsdExchangeStrategy implements ExchangeStrategy {

    @Override
    public void exchange(Account account, BigDecimal amount, BigDecimal rate) {
        account.setBalancePLN(account.getBalancePLN().subtract(amount));
        account.setBalanceUSD(account.getBalanceUSD().add(amount.divide(rate, 4, RoundingMode.HALF_EVEN)));
    }
}