package com.example.bankservice.strategy;

import com.example.bankservice.model.Account;

import java.math.BigDecimal;

public interface ExchangeStrategy {

    void exchange(Account account, BigDecimal amount, BigDecimal rate);
}
