package com.example.bankservice.strategy;

import com.example.bankservice.model.Account;

import java.math.BigDecimal;

public class UsdToPlnExchangeStrategy implements ExchangeStrategy {

    @Override
    public void exchange(Account account, BigDecimal amount, BigDecimal rate) {
        account.setBalanceUSD(account.getBalanceUSD().subtract(amount));
        account.setBalancePLN(account.getBalancePLN().add(amount.multiply(rate)));
    }
}