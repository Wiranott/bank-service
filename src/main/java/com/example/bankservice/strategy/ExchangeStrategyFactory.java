package com.example.bankservice.strategy;

import com.example.bankservice.model.Enum.Currency;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class ExchangeStrategyFactory {

    private final Map<String, ExchangeStrategy> strategies = new HashMap<>();

    public ExchangeStrategyFactory() {
        strategies.put(getKey(Currency.PLN, Currency.USD), new PlnToUsdExchangeStrategy());
        strategies.put(getKey(Currency.USD, Currency.PLN), new UsdToPlnExchangeStrategy());
    }

    public ExchangeStrategy getStrategy(Currency fromCurrency, Currency toCurrency) {
        return strategies.get(getKey(fromCurrency, toCurrency));
    }

    private String getKey(Currency fromCurrency, Currency toCurrency) {
        return fromCurrency.name() + "_" + toCurrency.name();
    }
}
