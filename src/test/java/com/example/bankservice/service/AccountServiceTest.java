package com.example.bankservice.service;

import com.example.bankservice.dto.AccountDTO;
import com.example.bankservice.dto.ExchangeDTO;
import com.example.bankservice.exception.InsufficientFundsException;
import com.example.bankservice.exception.ResourceNotFoundException;
import com.example.bankservice.model.Account;
import com.example.bankservice.model.Enum.Currency;
import com.example.bankservice.repository.AccountRepository;
import com.example.bankservice.strategy.ExchangeStrategy;
import com.example.bankservice.strategy.ExchangeStrategyFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class AccountServiceTest {

    @InjectMocks
    private AccountService accountService;

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private ExchangeRateService exchangeRateService;

    @Mock
    private ExchangeStrategyFactory exchangeStrategyFactory;

    @Mock
    private ExchangeStrategy exchangeStrategy;

    private Account account;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        account = new Account();
        account.setPesel("12345678901");
        account.setFirstName("John");
        account.setLastName("Doe");
        account.setBalancePLN(new BigDecimal("1000.00"));
        account.setBalanceUSD(new BigDecimal("0.00"));
    }

    @Test
    public void testCreateAccount() {
        var accountDTO = new AccountDTO();
        accountDTO.setPesel("12345678901");
        accountDTO.setFirstName("John");
        accountDTO.setLastName("Doe");
        accountDTO.setInitialBalancePLN(new BigDecimal("1000.00"));

        when(accountRepository.existsById(accountDTO.getPesel())).thenReturn(false);
        when(accountRepository.save(any(Account.class))).thenReturn(account);

        Account createdAccount = accountService.createAccount(accountDTO);
        assertNotNull(createdAccount);
        assertEquals("12345678901", createdAccount.getPesel());
    }

    @Test
    public void testCreateAccountAlreadyExists() {
        var accountDTO = new AccountDTO();
        accountDTO.setPesel("12345678901");
        accountDTO.setFirstName("John");
        accountDTO.setLastName("Doe");
        accountDTO.setInitialBalancePLN(new BigDecimal("1000.00"));

        when(accountRepository.existsById(accountDTO.getPesel())).thenReturn(true);

        assertThrows(ResourceNotFoundException.class, () -> accountService.createAccount(accountDTO));
    }

    @Test
    public void testGetAccount() {
        when(accountRepository.findById("12345678901")).thenReturn(Optional.of(account));

        var foundAccount = accountService.getAccount("12345678901");
        assertNotNull(foundAccount);
        assertEquals("12345678901", foundAccount.getPesel());
    }

    @Test
    public void testGetAccountNotFound() {
        when(accountRepository.findById("12345678901")).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> accountService.getAccount("12345678901"));
    }

    @Test
    public void testExchangeCurrencyPlnToUsd() {
        var exchangeDTO = new ExchangeDTO();
        exchangeDTO.setFromCurrency("PLN");
        exchangeDTO.setToCurrency("USD");
        exchangeDTO.setAmount(new BigDecimal("100.00"));

        when(accountRepository.findById("12345678901")).thenReturn(Optional.of(account));
        when(exchangeStrategyFactory.getStrategy(Currency.PLN, Currency.USD)).thenReturn(exchangeStrategy);
        when(exchangeRateService.getExchangeRate("PLN", "USD")).thenReturn(new BigDecimal("0.25"));

        accountService.exchangeCurrency("12345678901", exchangeDTO);

        verify(exchangeStrategy).exchange(account, new BigDecimal("100.00"), new BigDecimal("0.25"));
        verify(accountRepository).save(account);
    }

    @Test
    public void testExchangeCurrencyUsdToPln() {
        account.setBalanceUSD(new BigDecimal("100.00"));

        var exchangeDTO = new ExchangeDTO();
        exchangeDTO.setFromCurrency("USD");
        exchangeDTO.setToCurrency("PLN");
        exchangeDTO.setAmount(new BigDecimal("50.00"));

        when(accountRepository.findById("12345678901")).thenReturn(Optional.of(account));
        when(exchangeStrategyFactory.getStrategy(Currency.USD, Currency.PLN)).thenReturn(exchangeStrategy);
        when(exchangeRateService.getExchangeRate("USD", "PLN")).thenReturn(new BigDecimal("4.00"));

        accountService.exchangeCurrency("12345678901", exchangeDTO);

        verify(exchangeStrategy).exchange(account, new BigDecimal("50.00"), new BigDecimal("4.00"));
        verify(accountRepository).save(account);
    }

    @Test
    public void testExchangeCurrencyInsufficientFundsPln() {
        var exchangeDTO = new ExchangeDTO();
        exchangeDTO.setFromCurrency("PLN");
        exchangeDTO.setToCurrency("USD");
        exchangeDTO.setAmount(new BigDecimal("2000.00"));

        when(accountRepository.findById("12345678901")).thenReturn(Optional.of(account));

        assertThrows(InsufficientFundsException.class, () -> accountService.exchangeCurrency("12345678901", exchangeDTO));
    }

    @Test
    public void testExchangeCurrencyInsufficientFundsUsd() {
        account.setBalanceUSD(new BigDecimal("100.00"));

        var exchangeDTO = new ExchangeDTO();
        exchangeDTO.setFromCurrency("USD");
        exchangeDTO.setToCurrency("PLN");
        exchangeDTO.setAmount(new BigDecimal("200.00"));

        when(accountRepository.findById("12345678901")).thenReturn(Optional.of(account));

        assertThrows(InsufficientFundsException.class, () -> accountService.exchangeCurrency("12345678901", exchangeDTO));
    }
}
