package com.example.bankservice.validation;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AdultValidatorTest {
    private final AdultValidator validator = new AdultValidator();

    @Test
    void testValidAdultPesel() {
        assertTrue(validator.isValid("99012212345", null), "Should be valid and adult.");
    }

    @Test
    void testInvalidPeselFormat() {
        assertFalse(validator.isValid("9901221234", null), "Should be invalid due to incorrect length.");
        assertFalse(validator.isValid("abcde123456", null), "Should be invalid due to non-numeric characters.");
    }

    @Test
    void testValidNonAdultPesel() {
        assertFalse(validator.isValid("11250864135", null), "Should be valid but not adult.");
    }

    @Test
    void testNullPesel() {
        assertFalse(validator.isValid(null, null), "Should be invalid because PESEL is null.");
    }

    @Test
    void testFutureDates() {
        assertFalse(validator.isValid("22220212345", null), "Should be invalid due to birth date in the future.");
    }

    @Test
    void testBoundaryConditionAdultToday() {
        var pesel = generatePesel18YearsAgo();
        assertTrue(validator.isValid(pesel, null), "Should be adult as of today.");
    }

    private String generatePesel18YearsAgo() {
        var eighteenYearsAgo = LocalDate.now().minusYears(18);
        var year = eighteenYearsAgo.getYear();
        var month = eighteenYearsAgo.getMonthValue();
        var day = eighteenYearsAgo.getDayOfMonth();
        return String.format("%02d%02d%02d12345", year % 100, month, day);
    }
}
