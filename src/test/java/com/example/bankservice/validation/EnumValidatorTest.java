package com.example.bankservice.validation;

import com.example.bankservice.model.Enum.Currency;
import jakarta.validation.ConstraintValidatorContext;
import jakarta.validation.Payload;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.lang.annotation.Annotation;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EnumValidatorTest {

    private final EnumValidator enumValidator;

    @Mock
    private ConstraintValidatorContext context;

    public EnumValidatorTest() {
        MockitoAnnotations.openMocks(this);
        enumValidator = new EnumValidator();

        ValidEnum validEnumAnnotation = new ValidEnum() {
            @Override
            public Class<? extends Annotation> annotationType() {
                return ValidEnum.class;
            }

            @Override
            public String message() {
                return "Invalid value. This is not permitted.";
            }

            @Override
            public Class<?>[] groups() {
                return new Class[0];
            }

            @Override
            public Class<? extends Payload>[] payload() {
                return new Class[0];
            }

            @Override
            public Class<? extends Enum<?>> enumClass() {
                return Currency.class;
            }
        };

        enumValidator.initialize(validEnumAnnotation);
    }

    @Test
    public void testIsValid_NullValue() {
        assertTrue(enumValidator.isValid(null, context));
    }

    @Test
    public void testIsValid_ValidValue() {
        assertTrue(enumValidator.isValid("USD", context));
        assertTrue(enumValidator.isValid("PLN", context));
    }

    @Test
    public void testIsValid_InvalidValue() {
        assertFalse(enumValidator.isValid("INVALID", context));
        assertFalse(enumValidator.isValid("VALUE4", context));
    }
}