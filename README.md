# Bank Service

Bank Service to aplikacja zbudowana w oparciu o Spring Boot, umożliwiająca zarządzanie kontami bankowymi oraz przeprowadzanie operacji wymiany walutowej.


## Funkcjonalności

- Tworzenie kont bankowych
- Przeglądanie szczegółów konta bankowego
- Przeliczanie walut (PLN <-> USD)

## Technologie
- Java
- Spring Boot
- Hibernate (JPA)
- MySQL
- Liquibase
- Lombok

### Wymagania
 Java 21
- MySQL
- Maven

### Konfiguracja
- Skonfiguruj bazę danych MySQL
- Utwórz bazę danych `bankdb`
- Utwórz użytkownika i nadaj mu odpowiednie uprawnienia do bazy `bankdb`

### Profile uruchomieniowe

Aplikacja używa różnych profili uruchomieniowych do różnych środowisk.

- **dev**: Używa bazy danych MySQL.
- **test**: Używa bazy danych H2 w pamięci.